﻿using System;
using System.Diagnostics;
using System.IO;
using LendFoundry.Ocr.Engine.Infrastructure.Serialization;
using LendFoundry.Ocr.Engine.TemplateProcessors;
using RestSharp;

namespace LendFoundry.Ocr.Engine.ConsoleApp
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var stopWatch = new Stopwatch();
            stopWatch.Start();
            Console.WriteLine("Processing started for bank statement");
            var task = new BankEngine()
                .HandleStatement(@"c:\temp\9379368_Bank_Statement.pdf");

            task.Wait();

            var serializer = new JsonSerializationService();

            dynamic bankStatement = new
            {
                BankAccount = task.Result,
                Description = "Data from statement extracted, parsed, ocred and transformed successfully.",
                CreatedBy = "OCR Engine",
                CreatedOn = DateTime.Now.ToShortDateString()
            };

            var serializedStatement = serializer.Serialize(bankStatement);
            File.WriteAllText(@"c:\temp\9379368_Bank_Statement.json", serializedStatement);

            stopWatch.Stop();

            Console.WriteLine("Processing time {0} seconds", stopWatch.Elapsed);
            Console.WriteLine("Requesting Cashflow Api and POSTing data");

            var request = new RestRequest
            {
                RequestFormat = DataFormat.Json,
                Method = Method.POST
            };
            request.AddParameter("application/json", serializedStatement, ParameterType.RequestBody);
            var cashflow = new CashflowApi();
            var response = cashflow.Execute(request);

            Console.WriteLine(response);
            Console.WriteLine("Press any key to exit...");
            Console.ReadLine();
        }
    }
}
