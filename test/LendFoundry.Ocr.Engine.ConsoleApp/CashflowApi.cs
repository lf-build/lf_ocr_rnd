﻿using RestSharp;

namespace LendFoundry.Ocr.Engine.ConsoleApp
{
    public class CashflowApi
    {
        private const string BaseUrl = "http://localhost:5000/cashflow";

        private const string AuthToken =
            "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJteS1hcHAiLCJpYXQiOiIyMDE1LTEwLTA5VDA1OjExOjI3LjUzNzc3MjdaIiwiZXhwIjpudWxsLCJzdWIiOm51bGwsInRlbmFudCI6Im15LXRlbmFudCIsInNjb3BlIjpudWxsfQ.vVcomx0eqAs8WvFWEB-ntIB8kaijAqEzXB1AC8bNvaM";

        public string Execute(RestRequest request)
        {
            var client = new RestClient(BaseUrl);
            request.AddHeader("Authorization", AuthToken);
            request.AddHeader("Content-Type", "application/json");
            var response = client.Execute(request);
            return response.Content;
        }
    }
}
