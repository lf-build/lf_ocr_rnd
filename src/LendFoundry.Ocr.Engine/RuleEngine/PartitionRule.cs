﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.Ocr.Engine.Formatters.OcrXml;
using LendFoundry.Ocr.Engine.Helpers;
using MoreLinq;

namespace LendFoundry.Ocr.Engine.RuleEngine
{
    public class PartitionRule : RuleSet, IRuleExecutor<IEnumerable<Region>, IEnumerable<IEnumerable<Region>>>
    {
        public override RuleType Type => RuleType.Partition;
        public string Hint { get; set; }
        public IEnumerable<IEnumerable<Region>> Execute(IEnumerable<Region> regions)
        {
            var totalRegions = regions.Count();
            var rememberRegions = new List<int>();

            var hints = Hint.Split(new[] { RuleConst.PipeSeparator }, StringSplitOptions.None);
            
            //rememberRegions.Add(regionCount);
            foreach (var hint in hints)
            {
                var regionCount = 1;

                foreach (var region in regions)
                {
                    var lines = region.Blocks
                        .SelectMany(block => block.Contents)
                        .Select(cb => cb.ToTextBlocks())
                        .Flatten();

                    if (lines.Any(line => line.Value.Contains(hint)))
                    {
                        rememberRegions.Add(regionCount);
                    }

                    regionCount++;
                }
            }
            rememberRegions.Add(++totalRegions);

            if (totalRegions > 1 && rememberRegions.Count > 1)
            {
                var batches = rememberRegions.OrderBy(n => n).Windowed(2);
                foreach (var batch in batches)
                {
                    var skipRegion = batch.First() - 1;
                    var takeRegion = batch.Last() - skipRegion - 1;
                    yield return regions.Skip(skipRegion).Take(takeRegion);
                }
            }
            else
            {
                yield return regions;
            }
        }
    }
}
