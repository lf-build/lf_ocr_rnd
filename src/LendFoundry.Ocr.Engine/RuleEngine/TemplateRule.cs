﻿namespace LendFoundry.Ocr.Engine.RuleEngine
{
    public class TemplateRule : ConditionalRule
    {
        public override RuleType Type { get; set; } = RuleType.Template;
        public string Template { get; set; }
    }
}