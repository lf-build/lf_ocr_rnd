using System.Collections.Generic;
using System.Linq;
using LendFoundry.Ocr.Engine.Formatters.OcrXml;

namespace LendFoundry.Ocr.Engine.RuleEngine
{
    public class BlockRule : RuleSet, IRuleExecutor<IEnumerable<Block>, IEnumerable<TextBlock>>
    {
        public override RuleType Type => RuleType.Block;
        public int Position { get; set; }
        public string SkipLines { get; set; }
        public string TakeLines { get; set; }

        public IEnumerable<TextBlock> Execute(IEnumerable<Block> blocks)
        {
            var posBlock = blocks.ElementAt(Position - 1);

            if (SkipLines.Contains(RuleConst.StarSeparator))
                return Enumerable.Empty<TextBlock>();

            if (TakeLines.Contains(RuleConst.StarSeparator))
                return posBlock.Contents.OfType<TextBlock>();

            var separator = RuleConst.ListSeparator.ToCharArray()[0];

            var skips = SkipLines.Split(separator).Select(int.Parse).ToArray();
            var takes = TakeLines.Split(separator).Select(int.Parse).ToArray();

            IEnumerable<TextBlock> lines = posBlock.Contents.OfType<TextBlock>();

            if (skips.Length == 1)
                lines = lines.Skip(skips[0]);

            if (takes.Length == 1)
                lines = lines.Take(takes[0]);

            if (skips.Length > 1)
                lines = lines.SkipWhile((t, i) => skips.Contains(i - 1));

            if (takes.Length > 1)
                lines = lines.TakeWhile((t, i) => takes.Contains(i - 1));

            return lines;
        }
    }
}