﻿using System;
using System.Collections.Generic;
using System.Linq;
using LendFoundry.Ocr.Engine.Formatters.OcrXml;

namespace LendFoundry.Ocr.Engine.RuleEngine
{
    public class ConditionalRule : RuleSet, IRuleExecutor<IEnumerable<TextBlock>, bool>
    {
        public override RuleType Type => RuleType.Condition;
        public string Hint { get; set; }

        public bool Execute(IEnumerable<TextBlock> lines)
        {
            return Hint.Split(new[] {RuleConst.PipeSeparator}, StringSplitOptions.None)
                .Select(hint => lines.Any(line => line.Value.Contains(hint)))
                .All(condition => condition);
        }
    }
}