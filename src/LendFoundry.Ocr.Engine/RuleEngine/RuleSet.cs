﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;

namespace LendFoundry.Ocr.Engine.RuleEngine
{
    public class RuleSet : IEqualityComparer<RuleSet>
    {
        [JsonConverter(typeof (StringEnumConverter))]
        public virtual RuleType Type { get; set; } = RuleType.Unknown;

        public string Name { get; set; }
        public object Rule { get; set; }
        public bool Repeat { get; set; }
        public bool Success { get; set; }
        public IDictionary<string, IEnumerable<string>> Result { get; set; }
        // syntatic sugar - helper methods
        public ExpressionRule ToExpressionRule => To<ExpressionRule>(this);
        public BlockRule ToBlockRule => To<BlockRule>(this);
        public LineRule ToLineRule => To<LineRule>(this);
        public RangeRule ToRangeRule => To<RangeRule>(this);
        public ConditionalRule ToConditionRule => To<ConditionalRule>(this);
        public TemplateRule ToTemplateRule => To<TemplateRule>(this);
        public PatternRule ToPatternRule => To<PatternRule>(this);
        public PartitionRule ToPartitionRule => To<PartitionRule>(this);

        public static T To<T>(RuleSet ruleSet) where T : RuleSet
        {
            var jObject = ruleSet.Rule as JObject;
            return jObject?.ToObject<T>();
        }

        public bool Equals(RuleSet x, RuleSet y)
        {
            return string.Compare(x.Name, y.Name, StringComparison.CurrentCultureIgnoreCase) == 0;
        }

        public int GetHashCode(RuleSet obj)
        {
            return obj.Name.GetHashCode();
        }
    }
}