﻿using System.Collections.Generic;

namespace LendFoundry.Ocr.Engine.RuleEngine
{
    public class RuleContext
    {
        public IDictionary<string, RuleSet> Rules { get; set; } = new Dictionary<string, RuleSet>();
    }
}