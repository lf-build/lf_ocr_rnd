﻿namespace LendFoundry.Ocr.Engine.RuleEngine
{
    public static class RuleConst
    {
        public const string StarSeparator = "*";
        public const string ListSeparator = ",";
        public const string PipeSeparator = "|";
        public const string OpenLineSeparator = "||";
        public const string CloseLineSeparator = "||";
        public const string Unknown = "unknown";
    }
}