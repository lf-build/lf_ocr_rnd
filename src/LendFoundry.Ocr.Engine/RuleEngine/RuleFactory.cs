﻿using System.Collections.Generic;
using System.IO;
using LendFoundry.Ocr.Engine.Infrastructure.Serialization;

namespace LendFoundry.Ocr.Engine.RuleEngine
{
    public class RuleFactory
    {
        public static IEnumerable<RuleSet> GetRules(string rulesPath)
        {
            var serializer = new JsonSerializationService();

            var content = File.ReadAllText(rulesPath);

            var ruleContext = serializer.Deserialize<RuleContext>(content);

            var rules = new List<RuleSet>();

            foreach (var rule in ruleContext.Rules)
            {
                var ruleSet = rule.Value;
                ruleSet.Name = rule.Key;
                rules.Add(ruleSet);
            }

            return rules;
        }
    }
}