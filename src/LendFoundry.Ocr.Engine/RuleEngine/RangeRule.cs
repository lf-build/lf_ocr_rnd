using System;
using System.Collections.Generic;
using System.Linq;
using LendFoundry.Ocr.Engine.Formatters.OcrXml;
using LendFoundry.Ocr.Engine.Helpers;

namespace LendFoundry.Ocr.Engine.RuleEngine
{
    public class RangeRule : RuleSet, IRuleExecutor<IEnumerable<TextBlock>, IEnumerable<TextBlock>>
    {
        public override RuleType Type => RuleType.Range;
        public string FromHint { get; set; }
        public string ToHint { get; set; }
        public string Pattern { get; set; }

        public IEnumerable<TextBlock> Execute(IEnumerable<TextBlock> lines)
        {
            var fromHints = FromHint.Split(new[] {RuleConst.PipeSeparator}, StringSplitOptions.None)
                .WithContext()
                .First();

            var lineIndexes = lines.Select((l, i) => new {Line = l, Index = i});

            var fromLines = lineIndexes
                .WithContext()
                .FirstOrDefault(triad =>
                {
                    if (triad.Current.Line != null)
                        return triad.Current.Line.Value.Contains(fromHints.Current);

                    return false;
                });

            if (fromLines == null)
                return Enumerable.Empty<TextBlock>();

            if (fromLines.HasNext && fromHints.HasNext)
            {
                var nextLine = fromLines.Next;
                if (!nextLine.Line.Value.Contains(fromHints.Next))
                    fromLines = lineIndexes
                        .WithContext()
                        .Skip(fromLines.Next.Index)
                        .FirstOrDefault(triad =>
                        {
                            if (triad.Current.Line != null)
                                return triad.Current.Line.Value.Contains(fromHints.Current);

                            return false;
                        });
            }

            if (fromLines == null)
                return Enumerable.Empty<TextBlock>();

            //var toLine = lineIndexes.Last(l => l.Line.Value.Contains(ToHint)).Index;
            var toLine = lineIndexes
                .Skip(fromLines.Current.Index)
                .First(l => l.Line.Value.Contains(ToHint));

            if (toLine == null)
                return Enumerable.Empty<TextBlock>();

            var ranges = lines.Skip(fromLines.Current.Index).Take(toLine.Index - fromLines.Current.Index + 1);

            return new LineRule {Pattern = Pattern}.Execute(ranges);
        }
    }
}