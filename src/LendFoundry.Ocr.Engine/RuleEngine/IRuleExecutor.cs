﻿namespace LendFoundry.Ocr.Engine.RuleEngine
{
    public interface IRuleExecutor<in T, out TResult>
    {
        TResult Execute(T value);
    }
}