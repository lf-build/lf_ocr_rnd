using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using LendFoundry.Ocr.Engine.Formatters.OcrXml;

namespace LendFoundry.Ocr.Engine.RuleEngine
{
    public class ExpressionRule : RuleSet, IRuleExecutor<IEnumerable<TextBlock>, TextBlock>
    {
        public override RuleType Type => RuleType.Expression;
        public string Hint { get; set; }
        public string Pattern { get; set; }

        public TextBlock Execute(IEnumerable<TextBlock> lines)
        {
            var hints = Hint.Split(new[] {RuleConst.PipeSeparator}, StringSplitOptions.None);

            foreach (var line in lines)
            {
                var regex = new Regex(Pattern);

                if (hints.Select(hint => hint).All(hint => line.Value.Contains(hint)))
                {
                    var match = regex.Match(line.Value);
                    if (match.Success)
                        return new TextBlock
                        {
                            Value = regex.Match(line.Value).Value
                        };
                }
            }
            return TextBlock.None;
        }
    }
}