using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using LendFoundry.Ocr.Engine.Formatters.OcrXml;

namespace LendFoundry.Ocr.Engine.RuleEngine
{
    public class LineRule : RuleSet, IRuleExecutor<IEnumerable<TextBlock>, IEnumerable<TextBlock>>
    {
        public override RuleType Type => RuleType.Line;
        public string Pattern { get; set; }

        public IEnumerable<TextBlock> Execute(IEnumerable<TextBlock> lines)
        {
            var regex = new Regex(Pattern);
            var texts = new List<TextBlock>();
            foreach (var line in lines)
            {
                var match = regex.Match(line.Value);

                if (match.Success)
                {
                    var sb = new StringBuilder();
                    for (var i = 1; i < match.Groups.Count; i++)
                    {
                        sb.Append(RuleConst.OpenLineSeparator + match.Groups[i].Value + RuleConst.CloseLineSeparator);
                    }
                    texts.Add(new TextBlock {Value = sb.ToString()});
                }
            }

            return texts;
        }
    }
}