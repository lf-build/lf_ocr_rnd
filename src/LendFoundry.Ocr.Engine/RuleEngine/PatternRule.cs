﻿namespace LendFoundry.Ocr.Engine.RuleEngine
{
    public class PatternRule : RuleSet
    {
        public override RuleType Type => RuleType.Pattern;
        public string Pattern { get; set; }
    }
}
