﻿using System.ComponentModel;

namespace LendFoundry.Ocr.Engine.RuleEngine
{
    public enum RuleType
    {
        [Description("expression")] Expression,
        [Description("block")] Block,
        [Description("range")] Range,
        [Description("line")] Line,
        [Description("condition")] Condition,
        [Description("template")] Template,
        [Description("pattern")] Pattern,
        [Description("partition")] Partition,
        [Description("unknown")] Unknown
    }
}