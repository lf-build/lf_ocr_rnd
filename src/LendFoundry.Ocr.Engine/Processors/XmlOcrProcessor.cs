using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using LendFoundry.Ocr.Engine.Formatters.OcrXml;
using LendFoundry.Ocr.Engine.Formatters.RawXml;
using LendFoundry.Ocr.Engine.Helpers;
using MoreLinq;

namespace LendFoundry.Ocr.Engine.Processors
{
    public class XmlOcrProcessor : IOcrProcessor
    {
        private readonly IOcrExecutor _executor;
        private const int UnKnownOrder = -1;
        private const int MergeThreshold = 30;

        public XmlOcrProcessor() : this(new XmlOcrExecutor())
        {
        }

        public XmlOcrProcessor(IOcrExecutor executor)
        {
            _executor = executor;
        }

        private Func<string, string> GetOcrXmlFilePath
            => path => $"{Path.GetDirectoryName(path)}\\{Path.GetFileNameWithoutExtension(path)}.ocr.xml";

        public async Task<Container> Process(string inputPath)
        {
            var document = await _executor.Execute(inputPath);
            var container = default(Container);

            if (document != null)
            {
                Preprocess(document);
                container = Postprocess(document);

                var xmlserializer = new XmlSerializer(typeof(Container));
                using (var stringWriter = new StringWriter())
                using (var writer = XmlWriter.Create(stringWriter, new XmlWriterSettings { Indent = true }))
                {
                    xmlserializer.Serialize(writer, container);
                    File.WriteAllText(GetOcrXmlFilePath(inputPath), stringWriter.ToString());
                }
            }

            return await Task.FromResult(container);
        }

        private void Preprocess(Document document)
        {
            var pages = document.Pages;
            foreach (var page in pages)
            {
                TextZoneShaper(page);
                TableZoneShaper(page);
            }
        }

        private Container Postprocess(Document document)
        {
            var pages = document.Pages;
            var container = new Container();
            foreach (var page in pages)
            {
                var region = new Region();

                foreach (var zone in page.PageZone.Zones)
                {
                    // Process text zones
                    var textZone = zone as TextZone;

                    if (textZone != null)
                    {
                        var block = new Block();

                        foreach (var line in textZone.Lines)
                        {
                            block.Contents.Add(new TextBlock
                            {
                                Value = line.ToString()
                            });
                        }
                        region.Blocks.Add(block);
                    }

                    // Process table zones
                    var tableZone = zone as TableZone;

                    if (tableZone != null)
                    {
                        var block = new Block();
                        var table = new TableBlock
                        {
                            Columns = tableZone.Table.Columns.Count,
                            Rows = tableZone.Table.Rows.Count
                        };

                        foreach (var cell in tableZone.Cells)
                        {
                            var column = int.Parse(cell.ColumnFrom);
                            var row = int.Parse(cell.RowFrom);
                            var columnSpan = int.Parse(cell.ColumnTill);
                            var rowSpan = int.Parse(cell.RowTill);
                            var cellBlock = new CellBlock
                            {
                                Column = column,
                                Row = row,
                                ColumnSpan = columnSpan - column,
                                ColumnSpanSpecified = columnSpan - column != 0,
                                RowSpan = rowSpan - row,
                                RowSpanSpecified = rowSpan - row != 0,
                            };

                            foreach (var line in cell.Lines)
                            {
                                cellBlock.Texts.Add(new TextBlock
                                {
                                    Value = line.ToString(),
                                    Order = line.Order == UnKnownOrder ? 1 : line.Order,
                                    OrderSpecified = true
                                });
                            }

                            table.Cells.Add(cellBlock);
                        }

                        block.Contents.Add(table);
                        region.Blocks.Add(block);
                    }
                }

                container.Regions.Add(region);
            }
            return container;
        }

        private void TextZoneShaper(Page page)
        {
            var textZones = page.PageZone.Zones.OfType<TextZone>()
                .OrderBy(text => text.Top)
                .ThenBy(text => text.Left);

            if (textZones.Count() > 1)
            {
                // Merge text zones
                var rememberTextsToRemove = new List<TextZone>();

                var skipTextCount = 1;
                foreach (var currentText in textZones)
                {
                    var matchWithTexts = textZones.Skip(skipTextCount);
                    foreach (var nextText in matchWithTexts)
                    {
                        if (Math.Abs(nextText.Bottom - currentText.Bottom) < MergeThreshold)
                        {
                            var concatedLines =
                                currentText.Lines.Concat(nextText.Lines)
                                    .OrderBy(line => line.Bottom)
                                    .ThenBy(line => line.Left)
                                    .ToList();

                            currentText.Lines.Clear();
                            currentText.Lines.AddRange(concatedLines);
                            nextText.Lines.Clear();
                            rememberTextsToRemove.Add(nextText);
                        }
                    }
                    skipTextCount++;
                }

                rememberTextsToRemove.ForEach(text => page.PageZone.Zones.Remove(text));

                // Shift lines to next text zone if it is out of bounds in coords with current text zone
                var windowedTexts =
                    textZones.Windowed(2).Select(texts => new { First = texts.ElementAt(0), Second = texts.ElementAt(1) });

                foreach (var windowedText in windowedTexts)
                {
                    var rememberLinesToRemove = new List<Line>();
                    var currentText = windowedText.First;
                    var nextText = windowedText.Second;

                    foreach (var currentLine in windowedText.First.Lines)
                    {
                        if ((currentLine.Top > currentText.Top && currentLine.Bottom > currentText.Bottom) ||
                            (currentLine.Left > currentText.Left && currentLine.Right > currentText.Right &&
                             currentLine.Top < currentText.Top))
                        {
                            nextText.Lines.Add(currentLine.Copy());
                            currentLine.Words.Clear();
                            rememberLinesToRemove.Add(currentLine);
                        }
                    }

                    rememberLinesToRemove.ForEach(line => currentText.Lines.Remove(line));
                } 
            }

            // Merge lines in text zone
            foreach (var currentText in textZones)
            {
                if (currentText.Lines.Count > 1)
                {
                    LineShaper(currentText.Lines);
                }
            }
        }

        private void TableZoneShaper(Page page)
        {
            var tableZones = page.PageZone.Zones.OfType<TableZone>();

            foreach (var tableZone in tableZones)
            {
                // Check for table alignment if not align cells properly
                var mergeCells = tableZone.Cells.Where(cell => Math.Abs(int.Parse(cell.RowFrom) - int.Parse(cell.RowTill)) > 0);
                if (mergeCells.Any())
                {
                    foreach (var mergeFromCell in mergeCells)
                    {
                        var mergeToCell = tableZone.Cells.FirstOrDefault(cell => cell.RowFrom == mergeFromCell.RowTill);
                        if (mergeToCell != null)
                        {
                            mergeToCell.Lines.AddRange(mergeFromCell.Lines.Copy());
                            mergeFromCell.Lines.Clear();
                        }
                    }
                }

                var batches =
                    tableZone.Cells.GroupBy(cell => cell.RowFrom, (key, cells) => new {Cells = cells})
                        .Select(cell => cell.Cells);

                foreach (var batch in batches)
                {
                    var skipCell = 1;
                    foreach (var currentCell in batch)
                    {
                        // Merge lines
                        if (currentCell.Lines.Count > 1)
                        {
                            LineShaper(currentCell.Lines);
                        }

                        var currentOrder = currentCell.Lines.Count;

                        if (currentCell.Lines.Count == 0)
                        {
                            var emptyLine = Cell.None;
                            emptyLine.Order = 1;
                            currentCell.Lines.Add(emptyLine);
                            currentOrder = 1;
                        }

                        currentCell.Lines.ForEach((line, i) => { if (line.Order <= 0) line.Order = i + 1; });

                        foreach (var nextCell in batch.Skip(skipCell))
                        {
                            // Merge lines
                            if (nextCell.Lines.Count > 1)
                            {
                                LineShaper(nextCell.Lines);
                            }

                            var tagged = 0;
                            var rememberLinesTagged = new List<int>();

                            foreach (var currentLine in currentCell.Lines)
                            {
                                if (nextCell.Lines.Count == 0)
                                {
                                    var emptyLine = Cell.None;
                                    emptyLine.Order = 1;
                                    nextCell.Lines.Add(emptyLine);
                                }

                                if (tagged != nextCell.Lines.Count)
                                {
                                    var nextLineIndex = 0;
                                    foreach (var nextLine in nextCell.Lines)
                                    {
                                        if (Math.Abs(nextLine.Bottom - currentLine.Bottom) < MergeThreshold * 2)
                                        {
                                            tagged++;
                                            rememberLinesTagged.Add(nextLineIndex);
                                            if (currentLine.Order == UnKnownOrder)
                                            {
                                                currentOrder++;
                                                currentLine.Order = currentOrder;
                                            }
                                            nextLine.Order = currentLine.Order;
                                        }
                                        nextLineIndex++;
                                    }
                                }
                            }

                            // try to process remaining lines which are not tagged due to bottom match
                            if (tagged != nextCell.Lines.Count)
                            {
                                var untaggedLines = nextCell.Lines.Where((line, index) => !rememberLinesTagged.Contains(index));
                                untaggedLines.ForEach(
                                    (untaggedLine, i) =>
                                    {
                                        if (untaggedLine.Order <= 0) untaggedLine.Order = UnKnownOrder;
                                    });
                            }
                        }
                        skipCell++;
                    }
                }
            }
        }

        private void LineShaper(List<Line> lines)
        {
            var rememberLinesToRemove = new List<Line>();
            var skipLineCount = 1;
            
            foreach (var currentLine in lines)
            {
                var matchWithLines = lines.Skip(skipLineCount);
                foreach (var nextLine in matchWithLines)
                {
                    if (Math.Abs(nextLine.Bottom - currentLine.Bottom) < MergeThreshold)
                    {
                        currentLine.Words.AddRange(nextLine.Words.Copy());
                        nextLine.Words.Clear();
                        rememberLinesToRemove.Add(nextLine);
                    }
                }
                skipLineCount++;
            }
            rememberLinesToRemove.ForEach(line => lines.Remove(line));
        }
    }
}