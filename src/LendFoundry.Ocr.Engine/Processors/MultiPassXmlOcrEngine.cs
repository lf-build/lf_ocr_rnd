﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Nuance.OmniPage.CSDK.ArgTypes;
using Nuance.OmniPage.CSDK.Objects;
using Document = LendFoundry.Ocr.Engine.Formatters.RawXml.Document;

namespace LendFoundry.Ocr.Engine.Processors
{
    public class MultiPassXmlOcrEngine : IOcrEngine
    {
        private Func<string, string> GetSubFolderPath =>
            path => $"{Path.GetDirectoryName(path)}\\{Path.GetFileNameWithoutExtension(path)}";

        private Func<string, string> GetFileFullPath =>
             path => $"{GetSubFolderPath(path)}\\{Path.GetFileNameWithoutExtension(path)}";

        private Func<string, int, string> GetXmlFilePath =>
            (path, index) => $"{GetFileFullPath(path)}_{index}.xml";

        private Func<string, int, string> GetImageFilePath =>
            (path, index) => $"{GetFileFullPath(path)}_{index}.bmp";

        private Action<string> CreateFolderWithFilePath =>
            path =>
            {
                var folderPath = GetSubFolderPath(path);
                if (Directory.Exists(folderPath))
                {
                    var files = Directory.GetFiles(folderPath);
                    foreach (var file in files)
                    {
                        File.Delete(file);
                    }
                }
                Directory.CreateDirectory(folderPath);
            };

        public Task<bool> Extract(string inputPath)
        {

            var tcs = new TaskCompletionSource<bool>();

            try
            {
                //Initialize Engine
                Nuance.OmniPage.CSDK.Objects.Engine.Init(null, null);

                //Create sub folder based on input path
                CreateFolderWithFilePath(inputPath);

                //Create a settings collection
                //Load the input file
                using (var setting = new SettingCollection())
                {
                    var imagePaths = new List<string>();

                    using (var inputImageFile = new ImageFile(inputPath, FILEOPENMODE.IMGF_READ, IMF_FORMAT.FF_SIZE, setting))
                    {
                        //Iterate through the pages
                        for (var i = 0; i < inputImageFile.PageCount; i++)
                        {
                            //Load a page from the input file
                            using (var page = new Page(inputImageFile, i, setting))
                            {
                                var imagePath = GetImageFilePath(inputPath, i);
                                //var image = page[IMAGEINDEX.II_CURRENT];
                                //image.Save(imagePath, IMF_FORMAT.FF_PNG, false, true);
                                var bwPage = page.ConvertToBW(IMG_CONVERSION.CNV_AUTO, 50, 50, IMG_RESENH.RE_NO);
                                bwPage.Erode(ERO_DIL_TYPE.ERO_DIL_4);
                                bwPage.Dilate(ERO_DIL_TYPE.ERO_DIL_4);
                                var image = bwPage[IMAGEINDEX.II_BW];
                                image.Save(imagePath, IMF_FORMAT.FF_BMP_NO, false, true);
                                imagePaths.Add(imagePath);
                            }
                        }
                    }

                    //Modify any recognition settings
                    setting.Languages.Current = new[] { LANGUAGES.LANG_ENG };
                    //setting.PageDescription = PAGEDESCRIPTION.LZ_COLUMN_AUTO | PAGEDESCRIPTION.LZ_TABLE_AUTO | PAGEDESCRIPTION.LZ_GRAPHICS_AUTO;
                    setting.DefaultRecognitionModule = RECOGNITIONMODULE.RM_OMNIFONT_PLUS3W;
                    setting.RMTradeoff = RMTRADEOFF.TO_ACCURATE;
                    setting.DTXTOutputformat = DTXTOUTPUTFORMATS.DTXT_XMLCOORD;
                    setting.NongriddedTableDetect = false;
                    //setting.ImfLoadFlags = 1;

                    for (var i = 0; i < imagePaths.Count; i++)
                    {
                        using (var inputImageFile = new ImageFile(imagePaths[i], FILEOPENMODE.IMGF_READ, IMF_FORMAT.FF_PNG, setting))
                        {
                            using (var page = new Page(inputImageFile, 0, setting))
                            {
                                //Pre-process the document (rotate, deskew, etc.)
                                var xmlPath = GetXmlFilePath(inputPath, i);
                                page.Preprocess();
                                page.Recognize(xmlPath);
                            }
                        }
                    }
                }

                //Close the engine.
                Nuance.OmniPage.CSDK.Objects.Engine.ForceQuit();
                tcs.SetResult(true);
            }
            catch (Exception ex)
            {
                tcs.SetException(ex);
            }

            return tcs.Task;
        }

        public Task<Document> Parse(string inputPath)
        {
            var tcs = new TaskCompletionSource<Document>();

            try
            {
                var xmlFiles = Directory.GetFiles(GetSubFolderPath(inputPath), "*.xml")
                    .OrderBy(name => name);
                var document = new Document();
                var xmlSerializer = new XmlSerializer(typeof(Document));

                foreach (var xmlFile in xmlFiles)
                {
                    using (var stream = new FileStream(xmlFile, FileMode.Open, FileAccess.Read))
                    {
                        var result = (Document)xmlSerializer.Deserialize(stream);
                        document.Pages.AddRange(result.Pages);
                    }
                }
                tcs.SetResult(document);
            }
            catch (Exception ex)
            {
                tcs.SetException(ex);
            }
            return tcs.Task;
        }
    }
}