﻿using System;
using System.Threading.Tasks;
using LendFoundry.Ocr.Engine.Formatters.RawXml;

namespace LendFoundry.Ocr.Engine.Processors
{
    public interface IOcrEngine
    {
        Task<bool> Extract(string inputPath);
        Task<Document> Parse(string inputPath);
    }
}