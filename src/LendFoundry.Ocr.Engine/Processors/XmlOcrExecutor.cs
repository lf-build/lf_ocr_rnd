﻿using System.Threading.Tasks;
using LendFoundry.Ocr.Engine.Formatters.RawXml;

namespace LendFoundry.Ocr.Engine.Processors
{
    public class XmlOcrExecutor : IOcrExecutor
    {
        private readonly IOcrEngine _engine;

        public XmlOcrExecutor() : this(new MultiPassXmlOcrEngine2())
        {
        }

        public XmlOcrExecutor(IOcrEngine engine)
        {
            _engine = engine;
        }

        public string ActualFilePath { get; private set; }

        public async Task<Document> Execute(string inputPath)
        {
            ActualFilePath = inputPath;

            var isOcred = await _engine.Extract(inputPath);

            if (isOcred)
            {
                return await _engine.Parse(inputPath);
            }

            return await Task.FromResult(default(Document));
        }
    }
}