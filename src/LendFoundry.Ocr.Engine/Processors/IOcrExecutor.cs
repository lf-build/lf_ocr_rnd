﻿using System.Threading.Tasks;
using LendFoundry.Ocr.Engine.Formatters.RawXml;

namespace LendFoundry.Ocr.Engine.Processors
{
    public interface IOcrExecutor
    {
        string ActualFilePath { get; }
        Task<Document> Execute(string inputPath);
    }
}