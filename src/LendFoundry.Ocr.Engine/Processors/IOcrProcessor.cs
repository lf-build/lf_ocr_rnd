﻿using System.Threading.Tasks;
using LendFoundry.Ocr.Engine.Formatters.OcrXml;

namespace LendFoundry.Ocr.Engine.Processors
{
    public interface IOcrProcessor
    {
        Task<Container> Process(string inputPath);
    }
}