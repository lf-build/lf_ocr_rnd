using System;
using System.IO;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Nuance.OmniPage.CSDK.ArgTypes;
using Nuance.OmniPage.CSDK.Objects;
using Document = LendFoundry.Ocr.Engine.Formatters.RawXml.Document;

namespace LendFoundry.Ocr.Engine.Processors
{
    public class SinglePassXmlOcrEngine : IOcrEngine
    {
        private Func<string, string> GetXmlFilePath
            => path => $"{Path.GetDirectoryName(path)}\\{Path.GetFileNameWithoutExtension(path)}.xml";

        public Task<bool> Extract(string inputPath)
        {
            var outputPath = GetXmlFilePath(inputPath);

            if (File.Exists(outputPath))
                File.Delete(outputPath);

            var tcs = new TaskCompletionSource<bool>();

            try
            {
                //Initialize Engine
                Nuance.OmniPage.CSDK.Objects.Engine.Init(null, null);

                //Create a settings collection
                //Load the input file
                using (var setting = new SettingCollection())
                using (
                    var inputImageFile = new ImageFile(inputPath, FILEOPENMODE.IMGF_READ, IMF_FORMAT.FF_SIZE, setting))
                {
                    //Modify any recognition settings
                    setting.Languages.Current = new[] { LANGUAGES.LANG_ENG };
                    setting.PageDescription = PAGEDESCRIPTION.LZ_COLUMN_AUTO | PAGEDESCRIPTION.LZ_TABLE_AUTO | PAGEDESCRIPTION.LZ_GRAPHICS_AUTO;
                    setting.DefaultRecognitionModule = RECOGNITIONMODULE.RM_OMNIFONT_PLUS3W;
                    setting.RMTradeoff = RMTRADEOFF.TO_ACCURATE;
                    setting.DTXTOutputformat = DTXTOUTPUTFORMATS.DTXT_XMLCOORD;
                    setting.NongriddedTableDetect = false;
                    setting.ImfLoadFlags = 1;

                    //Iterate through the pages
                    for (var i = 0; i < inputImageFile.PageCount; i++)
                    {
                        //Load a page from the input file
                        using (var page = new Page(inputImageFile, i, setting))
                        {
                            //Pre-process the document (rotate, deskew, etc.)
                            page.Preprocess();
                            page.Recognize(outputPath);
                        }
                    }
                }
                //Close the engine.
                Nuance.OmniPage.CSDK.Objects.Engine.ForceQuit();
                tcs.SetResult(true);
            }
            catch (Exception ex)
            {
                tcs.SetException(ex);
            }

            return tcs.Task;
        }

        public Task<Document> Parse(string inputPath)
        {
            var xmlInputPath = GetXmlFilePath(inputPath);

            var tcs = new TaskCompletionSource<Document>();

            try
            {
                var xmlSerializer = new XmlSerializer(typeof (Document));
                using (var stream = new FileStream(xmlInputPath, FileMode.Open, FileAccess.Read))
                {
                    var result = (Document) xmlSerializer.Deserialize(stream);
                    tcs.SetResult(result);
                }
            }
            catch (Exception ex)
            {
                tcs.SetException(ex);
            }
            return tcs.Task;
        }
    }
}