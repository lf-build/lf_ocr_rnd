﻿using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Serialization;
using LendFoundry.Ocr.Engine.Ghostscripts;
using LendFoundry.Ocr.Engine.Helpers;
//using Ghostscript.NET;
//using Ghostscript.NET.Rasterizer;
using Nuance.OmniPage.CSDK.ArgTypes;
using Nuance.OmniPage.CSDK.Objects;
using Document = LendFoundry.Ocr.Engine.Formatters.RawXml.Document;

namespace LendFoundry.Ocr.Engine.Processors
{
    public class MultiPassXmlOcrEngine2 : IOcrEngine
    {
        private Func<string, string> GetSubFolderPath =>
            path => $"{Path.GetDirectoryName(path)}\\{Path.GetFileNameWithoutExtension(path)}";

        private Func<string, string> GetFileFullPath =>
             path => $"{GetSubFolderPath(path)}\\{Path.GetFileNameWithoutExtension(path)}";

        private Func<string, int, string> GetXmlFilePath =>
            (path, index) => $"{GetFileFullPath(path)}_{index}.xml";

        private Func<string, int, string> GetImageFilePath =>
            (path, index) => $"{GetFileFullPath(path)}_{index}.png";

        private Action<string> CreateFolderWithFilePath =>
            path =>
            {
                var folderPath = GetSubFolderPath(path);
                if (Directory.Exists(folderPath))
                    Directory.Delete(folderPath, true);
                Directory.CreateDirectory(folderPath);
            };

        public Task<bool> Extract(string inputPath)
        {

            var tcs = new TaskCompletionSource<bool>();

            try
            {
                //Create sub folder based on input path
                CreateFolderWithFilePath(inputPath);

                var imagePaths = new List<string>();

                using (var ghostScript = new GhostScript(@"C:\Ghostscript\bin"))
                {
                    var outputFiles = ghostScript.Convert(GhostScript.OutputDevice.png16m,
                        new GhostScript.DeviceOption[] { },
                        inputPath,
                        GetSubFolderPath(inputPath),
                        string.Empty,
                        @"c:\temp\test",
                        300);
                    imagePaths.AddRange(outputFiles);
                }
                
                //Initialize Engine
                Nuance.OmniPage.CSDK.Objects.Engine.Init(null, null);

                //Create a settings collection
                //Load the input file
                using (var setting = new SettingCollection())
                {
                    //Modify any recognition settings
                    setting.Languages.Current = new[] { LANGUAGES.LANG_ENG };
                    setting.PageDescription = PAGEDESCRIPTION.LZ_COLUMN_AUTO | PAGEDESCRIPTION.LZ_TABLE_AUTO | PAGEDESCRIPTION.LZ_GRAPHICS_AUTO;
                    setting.DefaultRecognitionModule = RECOGNITIONMODULE.RM_OMNIFONT_PLUS3W;
                    setting.RMTradeoff = RMTRADEOFF.TO_ACCURATE;
                    setting.DTXTOutputformat = DTXTOUTPUTFORMATS.DTXT_XMLCOORD;
                    setting.NongriddedTableDetect = false;
                    setting.ImfLoadFlags = 1;

                    var setAfterPageOne = false;
                    for (var i = 0; i < imagePaths.Count; i++)
                    {
                        using (var inputImageFile = new ImageFile(imagePaths[i], FILEOPENMODE.IMGF_READ, IMF_FORMAT.FF_PNG, setting))
                        {
                            using (var page = new Page(inputImageFile, 0, setting))
                            {
                                //Pre-process the document (rotate, deskew, etc.)
                                var xmlPath = GetXmlFilePath(inputPath, i + 1);
                                page.Preprocess();
                                page.Recognize(xmlPath);
                            }
                        }
                        if (!setAfterPageOne)
                        {
                            setting.PageDescription = PAGEDESCRIPTION.LZ_COLUMN_AUTO | PAGEDESCRIPTION.LZ_TABLE_ONE | PAGEDESCRIPTION.LZ_GRAPHICS_AUTO;
                            setting.NongriddedTableDetect = true;
                            setAfterPageOne = true;
                        }
                    }
                }

                //Close the engine.
                Nuance.OmniPage.CSDK.Objects.Engine.ForceQuit();
                tcs.SetResult(true);
            }
            catch (Exception ex)
            {
                tcs.SetException(ex);
            }

            return tcs.Task;
        }

        public Task<Document> Parse(string inputPath)
        {
            var tcs = new TaskCompletionSource<Document>();

            try
            {
                var xmlFiles = Directory.GetFiles(GetSubFolderPath(inputPath), "*.xml")
                    .OrderByAlphaNumeric(xmlFile => xmlFile);

                var document = new Document();
                var xmlSerializer = new XmlSerializer(typeof(Document));

                foreach (var xmlFile in xmlFiles)
                {
                    using (var stream = new FileStream(xmlFile, FileMode.Open, FileAccess.Read))
                    {
                        var result = (Document)xmlSerializer.Deserialize(stream);
                        document.Pages.AddRange(result.Pages);
                    }
                }
                tcs.SetResult(document);
            }
            catch (Exception ex)
            {
                tcs.SetException(ex);
            }
            return tcs.Task;
        }
    }
}