﻿using System;
using System.Collections.Generic;

namespace LendFoundry.Ocr.Engine.Helpers
{
    public static class AnonymousComparer
    {
        public static IComparer<T> GetComparer<T>(T example, Comparison<T> comparison)
        {
            return new ComparerImpl<T>(comparison);
        }
        private class ComparerImpl<T> : IComparer<T>
        {
            private readonly Comparison<T> _comparison;
            public ComparerImpl(Comparison<T> comparison) { _comparison = comparison; }
            public int Compare(T x, T y) { return _comparison.Invoke(x, y); }
        }
    }
}
