﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace LendFoundry.Ocr.Engine.Helpers
{
    public class TriadNode<T>
    {
        public TriadNode(T current, T previous, T next)
        {
            Current = current;
            Previous = previous;
            Next = next;
        }

        public T Previous { get; }
        public T Next { get; }
        public T Current { get; private set; }

        public bool HasNext => Next != null;
        public bool HasPrevious => Previous != null;
    }

    public static class LinqExtensions
    {
        public static IEnumerable<TriadNode<T>>
            WithContext<T>(this IEnumerable<T> source)
        {
            var previous = default(T);
            var current = source.FirstOrDefault();

            foreach (var next in source.Union(new[] { default(T) }).Skip(1))
            {
                yield return new TriadNode<T>(current, previous, next);
                previous = current;
                current = next;
            }
        }

        public static IEnumerable<T> Merge<T>(this IEnumerable<T> first,
            IEnumerable<T> second, Func<T, T, T> operation)
        {
            using (var iter1 = first.GetEnumerator())
            using (var iter2 = second.GetEnumerator())
            {
                while (iter1.MoveNext())
                {
                    if (iter2.MoveNext())
                    {
                        yield return operation(iter1.Current, iter2.Current);
                    }
                    else {
                        yield return iter1.Current;
                    }
                }
                while (iter2.MoveNext())
                {
                    yield return iter2.Current;
                }
            }
        }

        public static IEnumerable<TResult> ZipMany<TSource, TResult>(this IEnumerable<IEnumerable<TSource>> source, Func<IEnumerable<TSource>, TResult> selector)
        {
            // ToList is necessary to avoid deferred execution
            var enumerators = source.Select(seq => seq.GetEnumerator()).ToList();
            try
            {
                while (true)
                {
                    foreach (var e in enumerators)
                    {
                        bool b = e.MoveNext();
                        if (!b) yield break;
                    }
                    // Again, ToList (or ToArray) is necessary to avoid deferred execution
                    yield return selector(enumerators.Select(e => e.Current).ToList());
                }
            }
            finally
            {
                foreach (var e in enumerators)
                    e.Dispose();
            }
        }

        public static IEnumerable<T> Yield<T>(this T item)
        {
            yield return item;
        }

        public static IEnumerable<T> Flatten<T>(this IEnumerable<IEnumerable<T>> source)
        {
            return source.SelectMany(items => items);
        }

        public static IEnumerable<T> OrderByAlphaNumeric<T>(this IEnumerable<T> source, Func<T, string> selector)
        {
            int max = source
                .SelectMany(i => Regex.Matches(selector(i), @"\d+").Cast<Match>().Select(m => (int?)m.Value.Length))
                .Max() ?? 0;

            return source.OrderBy(i => Regex.Replace(selector(i), @"\d+", m => m.Value.PadLeft(max, '0')));
        }
    }
}