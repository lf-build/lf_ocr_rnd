﻿using System.Collections.Generic;
using System.Linq;
using LendFoundry.Ocr.Engine.Helpers;

namespace LendFoundry.Ocr.Engine.Formatters.OcrXml
{
    public class ContentBlock
    {
    }

    public static class ContentBlockExtensions
    {
        public static IEnumerable<TextBlock> ToTextBlocks(this ContentBlock content)
        {
            var text = content as TextBlock;
            if (text != null)
            {
                yield return text;
            }

            var table = content as TableBlock;
            if (table != null)
            {
                var rows = table.GetAllRows();
                foreach (var row in rows)
                {
                    yield return row;
                }
            }
        } 
    }
}
