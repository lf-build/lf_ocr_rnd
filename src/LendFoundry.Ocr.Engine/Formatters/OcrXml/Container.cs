﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace LendFoundry.Ocr.Engine.Formatters.OcrXml
{
    [XmlRoot("container")]
    public class Container
    {
        [XmlElement("region")]
        public List<Region> Regions { get; set; } = new List<Region>();
    }
}