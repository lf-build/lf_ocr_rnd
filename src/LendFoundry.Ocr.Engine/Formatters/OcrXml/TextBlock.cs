﻿using System.ComponentModel;
using System.Xml.Serialization;

namespace LendFoundry.Ocr.Engine.Formatters.OcrXml
{
    public class TextBlock : ContentBlock
    {
        public static readonly TextBlock None = new TextBlock {Value = "[*]"};

        [XmlAttribute("order")]
        [DefaultValue(-1)]
        public int Order { get; set; }

        [XmlText]
        public string Value { get; set; } = string.Empty;

        [XmlIgnore]
        public bool OrderSpecified { get; set; }

        public override string ToString()
        {
            return Value;
        }
    }
}