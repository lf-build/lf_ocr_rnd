﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using LendFoundry.Ocr.Engine.Helpers;
using MoreLinq;

namespace LendFoundry.Ocr.Engine.Formatters.OcrXml
{
    public class TableBlock : ContentBlock
    {
        [XmlAttribute("columns")]
        public int Columns { get; set; }

        [XmlAttribute("rows")]
        public int Rows { get; set; }

        [XmlElement("cell", typeof(CellBlock))]
        public List<CellBlock> Cells { get; set; } = new List<CellBlock>();

        public string this[int rowIndex]
        {
            get
            {
                return GetRow(rowIndex)
                    .Aggregate(new StringBuilder(), (a, b) => a.Append(b), result => result.ToString());
            }
        }

        public string this[int rowIndex, int columnIndex]
        {
            get
            {
                return Cells.First(cell => cell.Row == rowIndex && cell.Column == columnIndex).ToString();
            }
        }

        public IEnumerable<TextBlock> GetAllRows()
        {
            for (int i = 0; i < Rows; i++)
            {
                var rows = GetRow(i);
                foreach (var row in rows)
                {
                    yield return row;
                }
            }
        }

        public IEnumerable<TextBlock> GetRow(int rowIndex)
        {
            var filteredCells = Cells
                .Where(cell => cell.Row == rowIndex);

            if (filteredCells.Count() > 1)
                return filteredCells
                    .SelectMany(cell => cell.Texts)
                    .GroupBy(text => text.Order, (order, texts) => new
                    {
                        Order = order,
                        Text = texts.Aggregate((a, b) => new TextBlock {Value = $"{a.Value} {b.Value}"})
                    })
                    .OrderBy(gc => gc.Order)
                    .Select(gc => gc.Text);

            return filteredCells.SelectMany(cell => cell.Texts);
        }
    }
}
