﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace LendFoundry.Ocr.Engine.Formatters.OcrXml
{
    public class Region
    {
        [XmlElement("block")]
        public List<Block> Blocks { get; set; } = new List<Block>();
    }
}