﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace LendFoundry.Ocr.Engine.Formatters.OcrXml
{
    public class CellBlock
    {
        [XmlAttribute("column")]
        public int Column { get; set; }
        [XmlAttribute("row")]
        public int Row { get; set; }
        [XmlAttribute("columnspan")]
        [DefaultValue(0)]
        public int ColumnSpan { get; set; }
        [XmlIgnore]
        public bool ColumnSpanSpecified { get; set; }
        [XmlAttribute("rowspan")]
        [DefaultValue(0)]
        public int RowSpan { get; set; }
        [XmlIgnore]
        public bool RowSpanSpecified { get; set; }
        [XmlElement("text", typeof(TextBlock))]
        public List<TextBlock> Texts { get; set; } = new List<TextBlock>();
        public override string ToString()
        {
            return Texts.Aggregate(new StringBuilder(), (a, b) => a.Append(b.Value), result => result.ToString());
        }
    }
}
