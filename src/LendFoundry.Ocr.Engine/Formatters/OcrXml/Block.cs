﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;

namespace LendFoundry.Ocr.Engine.Formatters.OcrXml
{
    public class Block
    {
        [XmlElement("text", typeof(TextBlock))]
        [XmlElement("table", typeof(TableBlock))]
        public List<ContentBlock> Contents { get; set; } = new List<ContentBlock>();
    }
}