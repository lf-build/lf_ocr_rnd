using System.Xml.Serialization;

namespace LendFoundry.Ocr.Engine.Formatters.RawXml
{
    public class PageSource
    {
        [XmlAttribute("file")]
        public string File { get; set; }

        [XmlAttribute("dpix")]
        public int DpiX { get; set; }

        [XmlAttribute("dpiy")]
        public int DpiY { get; set; }

        [XmlAttribute("sizex")]
        public int SizeX { get; set; }

        [XmlAttribute("sizey")]
        public int SizeY { get; set; }
    }
}