using System.Xml.Serialization;

namespace LendFoundry.Ocr.Engine.Formatters.RawXml
{
    public class PageDescription
    {
        [XmlElement("language")]
        public string Language { get; set; }

        [XmlElement("source")]
        public PageSource Source { get; set; }

        [XmlElement("theoreticalPage")]
        public PageDetail TheoreticalPage { get; set; }

        [XmlAttribute("backColor")]
        public string BackColor { get; set; }
    }
}