using System.Xml.Serialization;

namespace LendFoundry.Ocr.Engine.Formatters.RawXml
{
    public class Page
    {
        [XmlElement("description")]
        public PageDescription Description { get; set; }

        [XmlElement("zones")]
        public PageZone PageZone { get; set; }

        [XmlAttribute("ocr-vers")]
        public string Ocrvers { get; set; }

        [XmlAttribute("app-vers")]
        public string Appvers { get; set; }

        [XmlIgnore]
        public int Width => Description.Source.SizeX;

        [XmlIgnore]
        public int Height => Description.Source.SizeY;

        [XmlIgnore]
        public int DpiX => Description.Source.DpiX;

        [XmlIgnore]
        public int DpiY => Description.Source.DpiY;
    }
}