using System.Xml.Serialization;

namespace LendFoundry.Ocr.Engine.Formatters.RawXml
{
    public class Column
    {
        [XmlText]
        public string Value { get; set; } = string.Empty;
    }
}