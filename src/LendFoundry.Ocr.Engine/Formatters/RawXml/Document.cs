using System.Collections.Generic;
using System.Xml.Serialization;

namespace LendFoundry.Ocr.Engine.Formatters.RawXml
{
    [XmlRoot(ElementName = "document", Namespace = "http://www.scansoft.com/omnipage/xml/ssdoc-schema3.xsd")]
    public class Document
    {
        [XmlElement("summary")]
        public string Summary { get; set; }

        [XmlElement("page")]
        public List<Page> Pages { get; set; } = new List<Page>();
    }
}