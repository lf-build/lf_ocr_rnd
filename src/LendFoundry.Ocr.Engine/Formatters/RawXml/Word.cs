using System.Collections.Generic;
using System.Xml.Serialization;

namespace LendFoundry.Ocr.Engine.Formatters.RawXml
{
    public class Word : Side
    {
        [XmlElement("run", IsNullable = true)]
        public List<Run> Runs { get; set; } = new List<Run>();

        [XmlAttribute("bold")]
        public string Bold { get; set; }

        [XmlAttribute("italic")]
        public string Italic { get; set; }

        [XmlAttribute("fontSize")]
        public string FontSize { get; set; }

        [XmlText]
        public string Value { get; set; } = string.Empty;
    }
}