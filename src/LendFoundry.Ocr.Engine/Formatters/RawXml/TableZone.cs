using System.Collections.Generic;
using System.Xml.Serialization;

namespace LendFoundry.Ocr.Engine.Formatters.RawXml
{
    public class TableZone : Zone
    {
        [XmlElement("gridTable")]
        public Table Table { get; set; }

        [XmlElement("cellZone")]
        public List<Cell> Cells { get; set; } = new List<Cell>();

        [XmlAttribute("fillingMethod")]
        public string FillingMethod { get; set; }

        [XmlAttribute("recognitionModule")]
        public string RecognitionModule { get; set; }

        [XmlAttribute("chrFilter")]
        public string ChrFilter { get; set; }
    }
}