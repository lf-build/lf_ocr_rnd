using System.Xml.Serialization;

namespace LendFoundry.Ocr.Engine.Formatters.RawXml
{
    public class Tab
    {
        [XmlAttribute("position")]
        public string Position { get; set; }

        [XmlAttribute("leader")]
        public string Leader { get; set; }
    }
}