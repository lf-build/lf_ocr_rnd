using System.Collections.Generic;
using System.Xml.Serialization;

namespace LendFoundry.Ocr.Engine.Formatters.RawXml
{
    public class TextZone : Zone
    {
        [XmlElement("ln")]
        public List<Line> Lines { get; set; } = new List<Line>();

        [XmlAttribute("fillingMethod")]
        public string FillingMethod { get; set; }

        [XmlAttribute("recognitionModule")]
        public string RecognitionModule { get; set; }

        [XmlAttribute("chrFilter")]
        public string ChrFilter { get; set; }

        [XmlAttribute("backColor")]
        public string BackColor { get; set; }
    }
}