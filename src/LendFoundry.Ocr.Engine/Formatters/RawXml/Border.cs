using System.Xml.Serialization;

namespace LendFoundry.Ocr.Engine.Formatters.RawXml
{
    public class Border
    {
        [XmlAttribute("type")]
        public string Type { get; set; }

        [XmlAttribute("width")]
        public int Width { get; set; }

        [XmlAttribute("color")]
        public string Color { get; set; }
    }

    public class BottomBorder : Border
    {
    }

    public class LeftBorder : Border
    {
    }

    public class RightBorder : Border
    {
    }

    public class TopBorder : Border
    {
    }
}