using System;
using System.Xml.Serialization;

namespace LendFoundry.Ocr.Engine.Formatters.RawXml
{
    public class Side
    {
        [XmlAttribute("l")]
        public int Left { get; set; }

        [XmlAttribute("t")]
        public int Top { get; set; }

        [XmlAttribute("r")]
        public int Right { get; set; }

        [XmlAttribute("b")]
        public int Bottom { get; set; }

        [XmlIgnore]
        public int CalculatedWidth => Math.Abs(Right - Left);

        [XmlIgnore]
        public int CalculatedHeight => Math.Abs(Bottom - Top);
    }
}