using System.Collections.Generic;
using System.Xml.Serialization;

namespace LendFoundry.Ocr.Engine.Formatters.RawXml
{
    public class Table
    {
        [XmlElement("gridCol", IsNullable = true)]
        public List<Column> Columns { get; set; } = new List<Column>();

        [XmlElement("gridRow", IsNullable = true)]
        public List<Row> Rows { get; set; } = new List<Row>();
    }
}