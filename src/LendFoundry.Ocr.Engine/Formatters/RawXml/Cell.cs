using System.Collections.Generic;
using System.Xml.Serialization;
using System.Linq;
using System.Text;

namespace LendFoundry.Ocr.Engine.Formatters.RawXml
{
    public class Cell : Side
    {
        public static readonly Line None = new Line { Words = new List<Word> { new Word { Value = "[*]" } } };

        [XmlElement("topBorder")]
        public TopBorder[] TopBorder { get; set; }

        [XmlElement("bottomBorder")]
        public BottomBorder[] BottomBorder { get; set; }

        [XmlElement("ln")]
        public List<Line> Lines { get; set; } = new List<Line>();

        [XmlAttribute("gridColFrom")]
        public string ColumnFrom { get; set; }

        [XmlAttribute("gridColTill")]
        public string ColumnTill { get; set; }

        [XmlAttribute("gridRowFrom")]
        public string RowFrom { get; set; }

        [XmlAttribute("gridRowTill")]
        public string RowTill { get; set; }

        public override string ToString()
        {
            return Lines.OrderBy(line => line.Left)
                .Aggregate(new StringBuilder(), (a, b) => a.Append(b.ToString()), result => result.ToString());
        }

        public bool IsBlank()
        {
            return Lines.Count == 0 || Lines.All(line => string.IsNullOrEmpty(line.Words.ToString()));
        }
    }
}