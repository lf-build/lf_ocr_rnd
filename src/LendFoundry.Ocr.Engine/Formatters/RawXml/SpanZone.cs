using System.Xml.Serialization;

namespace LendFoundry.Ocr.Engine.Formatters.RawXml
{
    public class SpanZone : Zone
    {
        [XmlAttribute("type")]
        public string Type { get; set; }

        [XmlAttribute("width")]
        public int Width { get; set; }

        [XmlAttribute("color")]
        public string Color { get; set; }
    }
}