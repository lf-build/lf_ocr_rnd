using System.Xml.Serialization;

namespace LendFoundry.Ocr.Engine.Formatters.RawXml
{
    public class Run
    {
        [XmlAttribute]
        public string Subsuperscript { get; set; }

        [XmlAttribute]
        public string Bold { get; set; }

        [XmlAttribute]
        public string Italic { get; set; }

        [XmlText]
        public string Value { get; set; } = string.Empty;
    }
}