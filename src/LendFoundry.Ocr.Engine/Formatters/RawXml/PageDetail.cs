using System.Xml.Serialization;

namespace LendFoundry.Ocr.Engine.Formatters.RawXml
{
    public class PageDetail
    {
        [XmlAttribute("size")]
        public string Size { get; set; }

        [XmlAttribute("marginLeft")]
        public int MarginLeft { get; set; }

        [XmlAttribute("marginTop")]
        public int MarginTop { get; set; }

        [XmlAttribute("marginRight")]
        public int MarginRight { get; set; }

        [XmlAttribute("marginBottom")]
        public int MarginBottom { get; set; }

        [XmlAttribute("width")]
        public int Width { get; set; }

        [XmlAttribute("height")]
        public int Height { get; set; }
    }
}