using System.Xml.Serialization;

namespace LendFoundry.Ocr.Engine.Formatters.RawXml
{
    public class Space
    {
        [XmlAttribute("width")]
        public int Width { get; set; }
    }
}