﻿using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace LendFoundry.Ocr.Engine.Formatters.RawXml
{
    public class Line : Side
    {
        [XmlElement("wd")]
        public List<Word> Words { get; set; } = new List<Word>();

        [XmlElement("space")]
        public List<Space> Spaces { get; set; } = new List<Space>();

        [XmlElement("tab")]
        public List<Tab> Tabs { get; set; } = new List<Tab>();

        [XmlAttribute("baseLine")]
        public string BaseLine { get; set; }

        [XmlAttribute("underlined")]
        public string Underlined { get; set; }

        [XmlAttribute("fontSize")]
        public string FontSize { get; set; }

        [XmlAttribute("bold")]
        public string Bold { get; set; }

        [XmlAttribute("italic")]
        public string Italic { get; set; }

        [XmlAttribute("foreColor")]
        public string ForeColor { get; set; }

        [XmlIgnore]
        public int Order { get; set; }

        public override string ToString()
        {
            var sb = new StringBuilder();
            foreach (var word in Words)
            {
                sb.Append(word.Value);

                foreach (var run in word.Runs)
                {
                    sb.Append(run.Value);
                }

                sb.Append(" ");
            }
            return sb.ToString().Trim();
        }
    }
}