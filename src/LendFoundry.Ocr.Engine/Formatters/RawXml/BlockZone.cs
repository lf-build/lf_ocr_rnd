using System.Xml.Serialization;

namespace LendFoundry.Ocr.Engine.Formatters.RawXml
{
    public class BlockZone : Zone
    {
        [XmlElement("leftBorder")]
        public LeftBorder[] LeftBorder { get; set; }

        [XmlElement("topBorder")]
        public TopBorder[] TopBorder { get; set; }

        [XmlElement("rightBorder")]
        public RightBorder[] RightBorder { get; set; }

        [XmlElement("bottomBorder")]
        public BottomBorder[] BottomBorder { get; set; }

        [XmlAttribute("backColor")]
        public string BackColor { get; set; }
    }
}