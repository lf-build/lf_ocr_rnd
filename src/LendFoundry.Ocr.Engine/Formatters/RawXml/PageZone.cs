using System.Collections.Generic;
using System.Xml.Serialization;

namespace LendFoundry.Ocr.Engine.Formatters.RawXml
{
    public class PageZone
    {
        [XmlElement("textZone", typeof(TextZone))]
        [XmlElement("pictureZone", typeof(PictureZone))]
        [XmlElement("rulerline", typeof(SpanZone))]
        [XmlElement("tableZone", typeof(TableZone))]
        [XmlElement("zone", typeof(BlockZone))]
        public List<Zone> Zones { get; set; } = new List<Zone>();
    }
}