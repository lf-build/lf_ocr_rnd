using System.Xml.Serialization;

namespace LendFoundry.Ocr.Engine.Formatters.RawXml
{
    public class Row
    {
        [XmlText]
        public string Value { get; set; } = string.Empty;
    }
}