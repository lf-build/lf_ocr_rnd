﻿namespace LendFoundry.Ocr.Engine.Infrastructure.Serialization
{
    public interface ISerializationService
    {
        string Serialize<T>(T objectToSerialize) where T : class;
        T Deserialize<T>(string serializedObject) where T : class;
    }
}