﻿using Newtonsoft.Json;

namespace LendFoundry.Ocr.Engine.Infrastructure.Serialization
{
    public class JsonSerializationService : ISerializationService
    {
        public T Deserialize<T>(string serializedObject) where T : class
        {
            return JsonConvert.DeserializeObject<T>(serializedObject);
        }

        public string Serialize<T>(T objectToSerialize) where T : class
        {
            return JsonConvert.SerializeObject(objectToSerialize, Formatting.Indented);
        }
    }
}