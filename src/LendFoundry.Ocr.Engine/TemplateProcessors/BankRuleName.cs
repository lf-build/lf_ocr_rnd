﻿namespace LendFoundry.Ocr.Engine.TemplateProcessors
{
    public class BankRuleName
    {
        public const string IsCheckingAccount = "isCheckingAccount";
        public const string IsSavingAccount = "isSavingAccount";
        public const string IsMultipleAccount = "isMultipleAccount";
        public const string BankAddress = "bankAddress";
        public const string CustomerAddress = "customerAddress";
        public const string CustomerName = "customerName";
        public const string StatementMonth = "statementMonth";
        public const string AccountNumber = "accountNumber";
        public const string BeginningBalance = "beginningBalance";
        public const string DepositBalance = "depositBalance";
        public const string AtmAndDebitCardSubtractions = "atmAndDebitCardSubtractions";
        public const string OtherSubtractions = "otherSubtractions";
        public const string Checks = "checks";
        public const string Servicefees = "servicefees";
        public const string EndingBalance = "endingBalance";
        public const string DepositTransactions = "depositTransactions";
        public const string AtmAndDebitCardTransactions = "atmAndDebitCardTransactions";
        public const string OtherTransactions = "otherTransactions";
        public const string ServiceFeeTransactions = "serviceFeeTransactions";
        public const string AllTransactions = "allTransactions";
        public const string Pages = "pages";
    }
}
