﻿using System.Linq;
using System.Threading.Tasks;
using LendFoundry.Ocr.Engine.Cashflow;
using LendFoundry.Ocr.Engine.Formatters.OcrXml;
using LendFoundry.Ocr.Engine.Processors;
using LendFoundry.Ocr.Engine.RuleEngine;

namespace LendFoundry.Ocr.Engine.TemplateProcessors
{
    public class BankEngine : IBankEngine
    {
        private static readonly string BankRulesPath =
            @"C:\LendFoundry\lf_ocr_rnd\src\LendFoundry.Ocr.Engine\Templates\bank.json";

        private readonly IOcrProcessor _ocrProcessor;

        public BankEngine() : this(new XmlOcrProcessor())
        {
        }

        public BankEngine(IOcrProcessor ocrProcessor)
        {
            _ocrProcessor = ocrProcessor;
        }

        public async Task<BankStatement> HandleStatement(string statementPath)
        {
            var statement = await _ocrProcessor.Process(statementPath);

            var bankProcessor = GetBankProcessor(statement);

            return await bankProcessor.ProcessStatement(statement);
        }

        private static IBankProcessor GetBankProcessor(Container statement)
        {
            var banks = RuleFactory.GetRules(BankRulesPath);
            var texts = statement.Regions.SelectMany(r => r.Blocks.SelectMany(b => b.Contents.OfType<TextBlock>()));

            foreach (var bank in banks)
            {
                if (bank.Type == RuleType.Condition)
                {
                    if (bank.ToConditionRule.Execute(texts))
                        return BankProcessorFactory
                            .GetBank(bank.Name.ToLowerInvariant());
                }
            }

            return BankProcessorFactory.EmptyBankProcessor;
        }
    }
}