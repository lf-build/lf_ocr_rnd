﻿using System.Collections.Generic;

namespace LendFoundry.Ocr.Engine.TemplateProcessors
{
    public static class BankName
    {
        public const string BankOfAmerica = "bankofamerica";
        public const string JPMorganChase = "jpmorganchase";
        public const string CitiBank = "citibank";
        public const string WellsFargo = "wellsfargo";
        public const string HSBC = "hsbc";

        private static readonly IDictionary<string, string> BankNames = BuildBankNames();

        private static IDictionary<string, string> BuildBankNames()
        {
            return new Dictionary<string, string>
            {
                {BankOfAmerica, "Bank Of America"},
                {JPMorganChase, "JP Morgan Chase"},
                {CitiBank, "CitiBank"},
                {WellsFargo, "Wells Fargo"},
                {HSBC, "HSBC"},
            };
        }

        public static string Get(string bankName)
        {
            return BankNames[bankName];
        }
    }
}