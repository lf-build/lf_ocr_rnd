﻿using System.Collections.Generic;
using LendFoundry.Ocr.Engine.Cashflow;

namespace LendFoundry.Ocr.Engine.TemplateProcessors
{
    public class WellsFargoProcessor : BankProcessor
    {
        private static readonly string BankTemplateFolder = "WellsFargo";

        protected override string GetBankTemplateFolder()
        {
            return BankTemplateFolder;
        }

        protected override BankStatement TransformStatement(IEnumerable<IDictionary<string, IEnumerable<string>>> ruleResults)
        {
            throw new System.NotImplementedException();
        }
    }
}
