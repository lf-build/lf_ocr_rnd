﻿using System.Threading.Tasks;
using LendFoundry.Ocr.Engine.Cashflow;
using LendFoundry.Ocr.Engine.Formatters.OcrXml;

namespace LendFoundry.Ocr.Engine.TemplateProcessors
{
    public interface IBankProcessor
    {
        Task<BankStatement> ProcessStatement(Container statement);
    }
}