﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using LendFoundry.Ocr.Engine.Cashflow;
using LendFoundry.Ocr.Engine.Formatters.OcrXml;
using LendFoundry.Ocr.Engine.Helpers;
using LendFoundry.Ocr.Engine.RuleEngine;
using Newtonsoft.Json.Linq;

namespace LendFoundry.Ocr.Engine.TemplateProcessors
{
    public abstract class BankProcessor : IBankProcessor
    {
        private const string BaseTemplateFolder =
            @"C:\LendFoundry\lf_ocr_rnd\src\LendFoundry.Ocr.Engine\Templates\";

        private const string VersionRuleFileName = "version.json";
        private const string PatternRuleFileName = "pattern.json";

        private Func<string, string> GetBaseRulesPath => path => $"{BaseTemplateFolder}{path}";
        private Func<string, string, string> GetCurrentRulesPath
            => (folderName, fileName) => $"{GetBaseRulesPath(folderName)}\\{fileName}";

        public Task<BankStatement> ProcessStatement(Container statement)
        {
            var rules = GetTemplateRules(statement);
            var patterns = GetPatternRules();

            var result = new List<IDictionary<string, IEnumerable<string>>>();

            ApplyPatternRules(rules, patterns);

            if (rules.Any(rule => rule.Type == RuleType.Partition))
            {
                var nonRepetitiveRules = rules.Where(rule => !rule.Repeat && rule.Type != RuleType.Partition);
                result.Add(ProcessRules(nonRepetitiveRules, statement.Regions));

                var repetitiveRules = rules.Where(rule => rule.Repeat);
                var partitionRule = rules.FirstOrDefault(rule => rule.Type == RuleType.Partition);
                if (partitionRule != null)
                {
                    var partitionedRegions = partitionRule.ToPartitionRule.Execute(statement.Regions);
                    foreach (var partitionedRegion in partitionedRegions)
                    {
                        result.Add(ProcessRules(repetitiveRules, partitionedRegion));
                    }
                }
            }
            else
            {
                result.Add(ProcessRules(rules, statement.Regions));
            }
            
            return Task.FromResult(TransformStatement(result));
        }

        private IDictionary<string, IEnumerable<string>> ProcessRules(IEnumerable<RuleSet> rules, IEnumerable<Region> regions)
        {
            var blocks = regions.SelectMany(r => r.Blocks);

            var texts = blocks
                .SelectMany(block => block.Contents)
                .Select(cb => cb.ToTextBlocks())
                .Flatten();

            var ruleResults = new Dictionary<string, IEnumerable<string>>();

            foreach (var rule in rules)
            {
                switch (rule.Type)
                {
                    case RuleType.Expression:
                        var expressionResult = rule.ToExpressionRule.Execute(texts);
                        var stringifyExpression = new List<string> {expressionResult.ToString()};
                        ruleResults.Add(rule.Name, stringifyExpression);
                        break;
                    case RuleType.Block:
                        var blockResult = rule.ToBlockRule.Execute(blocks);
                        var stringifyBlock = blockResult.Select(result => result.ToString()).ToList();
                        ruleResults.Add(rule.Name, stringifyBlock);
                        break;
                    case RuleType.Range:
                        var rangeResult = rule.ToRangeRule.Execute(texts);
                        var stringifyRange = rangeResult.Select(result => result.ToString()).ToList();
                        ruleResults.Add(rule.Name, stringifyRange);
                        break;
                    case RuleType.Line:
                        var lineResult = rule.ToLineRule.Execute(texts);
                        var stringifyLine = lineResult.Select(result => result.ToString()).ToList();
                        ruleResults.Add(rule.Name, stringifyLine);
                        break;
                    case RuleType.Condition:
                        var conditionResult = rule.ToConditionRule.Execute(texts);
                        var stringifyCondition = new List<string> {conditionResult.ToString()};
                        ruleResults.Add(rule.Name, stringifyCondition);
                        break;
                    case RuleType.Unknown:
                        break;
                }
            }
            return ruleResults;
        } 

        protected abstract string GetBankTemplateFolder();

        protected abstract BankStatement TransformStatement(
            IEnumerable<IDictionary<string, IEnumerable<string>>> ruleResults);
         
        private IEnumerable<RuleSet> GetTemplateRules(Container statement)
        {
            var blocks = statement.Regions.SelectMany(r => r.Blocks);

            var texts = blocks
                .SelectMany(block => block.Contents)
                .Select(cb => cb.ToTextBlocks())
                .Flatten();

            var bankFolder = GetBankTemplateFolder();
            var versionRulesPath = GetCurrentRulesPath(bankFolder, VersionRuleFileName);
            var versionRules = RuleFactory.GetRules(versionRulesPath);

            foreach (var versionRule in versionRules)
            {
                if (versionRule.Type == RuleType.Template)
                {
                    var templateRule = versionRule.ToTemplateRule;
                    if (templateRule.Execute(texts))
                    {
                        var templateRulesPath = GetCurrentRulesPath(bankFolder, templateRule.Template);
                        return RuleFactory.GetRules(templateRulesPath);
                    }
                }
            }

            return Enumerable.Empty<RuleSet>();
        }

        private IEnumerable<RuleSet> GetPatternRules()
        {
            var bankFolder = GetBankTemplateFolder();
            var globalPatternsPath = GetBaseRulesPath(PatternRuleFileName);
            var currentPatternsPath = GetCurrentRulesPath(bankFolder, PatternRuleFileName);
            var globalRules = RuleFactory.GetRules(globalPatternsPath);
            var currentRules = RuleFactory.GetRules(currentPatternsPath);

            return currentRules.Concat(
                globalRules.Where(p =>
                    !currentRules.Any(r =>
                        string.Compare(r.Name, p.Name, StringComparison.InvariantCultureIgnoreCase) == 0)));
        }

        private void ApplyPatternRules(IEnumerable<RuleSet> rules, IEnumerable<RuleSet> patterns)
        {
            Func<string, string> applyPattern = pattern => patterns.First(
                p =>
                    string.Compare($"<{p.Name}>", pattern,
                        StringComparison.InvariantCultureIgnoreCase) == 0)
                .ToPatternRule.Pattern;

            var placeholder = new Regex("<\\w+>");

            foreach (var rule in rules)
            {
                Match match;
                switch (rule.Type)
                {
                    case RuleType.Expression:
                        var expression = rule.ToExpressionRule;
                        match = placeholder.Match(expression.Pattern);
                        if (match.Success)
                            expression.Pattern = applyPattern(expression.Pattern);
                        rule.Rule = JObject.FromObject(expression);
                        break;
                    case RuleType.Range:
                        var range = rule.ToRangeRule;
                        match = placeholder.Match(range.Pattern);
                        if (match.Success)
                            range.Pattern = applyPattern(range.Pattern);
                        rule.Rule = JObject.FromObject(range);
                        break;
                    case RuleType.Line:
                        var line = rule.ToLineRule;
                        match = placeholder.Match(line.Pattern);
                        if (match.Success)
                            line.Pattern = applyPattern(line.Pattern);
                        rule.Rule = JObject.FromObject(line);
                        break;
                }
            }

        }
    }
}