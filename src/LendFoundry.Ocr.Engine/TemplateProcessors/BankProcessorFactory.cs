﻿using System;
using System.Collections.Generic;

namespace LendFoundry.Ocr.Engine.TemplateProcessors
{
    public class BankProcessorFactory
    {
        private static readonly IDictionary<string, Func<IBankProcessor>> BankEngines
            = BuildBankEngines();

        public static IBankProcessor EmptyBankProcessor => new EmptyBankProcessor();

        public static IBankProcessor GetBank(string bankName)
        {
            return BankEngines[bankName]();
        }

        private static IDictionary<string, Func<IBankProcessor>> BuildBankEngines()
        {
            return new Dictionary<string, Func<IBankProcessor>>
            {
                {BankName.BankOfAmerica, () => new BankOfAmericaProcessor()},
                {BankName.JPMorganChase, () => new JPMorganChaseProcessor()},
                {BankName.CitiBank, () => new CitiBankProcessor()},
                {BankName.WellsFargo, () => new WellsFargoProcessor()},
                {BankName.HSBC, () => new HSBCProcessor()}
            };
        }
    }
}