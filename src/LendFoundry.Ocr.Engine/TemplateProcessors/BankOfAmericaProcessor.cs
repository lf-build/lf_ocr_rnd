﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using LendFoundry.Ocr.Engine.Cashflow;
using LendFoundry.Ocr.Engine.RuleEngine;

namespace LendFoundry.Ocr.Engine.TemplateProcessors
{
    public class BankOfAmericaProcessor : BankProcessor
    {
        private static readonly string BankTemplateFolder = "BankOfAmerica";

        private static readonly string MonthYearPattern =
            @"\b(?<month>(?:Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\w+)\s*(?:\d+[, ]+)(?<year>\d+)\b";

        protected override string GetBankTemplateFolder()
        {
            return BankTemplateFolder;
        }

        protected override BankStatement TransformStatement(IEnumerable<IDictionary<string, IEnumerable<string>>> ruleResults)
        {
            var bankStatementSummary = ruleResults.First();

            var bankStatement = TransformToBankStatement(bankStatementSummary);

            if (IsTrueFor(BankRuleName.IsMultipleAccount, bankStatementSummary))
            {
                foreach (var bankAccountSummary in ruleResults.Skip(1))
                {
                    var bankAccount = TranformToBankAccount(bankAccountSummary);
                    bankAccount.Customer = TransformToCustomer(bankStatementSummary);
                    bankStatement.BankAccounts.Add(bankAccount);
                }
            }
            else
            {
                var bankAccount = TranformToBankAccount(bankStatementSummary);
                bankAccount.Customer = TransformToCustomer(bankStatementSummary);
                bankStatement.BankAccounts.Add(bankAccount);
            }

            bankStatement.TotalBalance = bankStatement.BankAccounts.Aggregate(0.0M, (a, b) => a + b.EndingBalance);

            return bankStatement;
        }

        private List<StatementEntry> TransformToStatementEntries(IDictionary<string, IEnumerable<string>> ruleResults)
        {
            Func<string, DateTime> parseDate =
                text => DateTime.ParseExact(text, "MM/dd/yy", CultureInfo.InvariantCulture);

            var statementEntries = new List<StatementEntry>();
            var depositTransactions = ruleResults[BankRuleName.DepositTransactions];
            var transactionSpliter = new[] { RuleConst.CloseLineSeparator };
            var entryCounter = 0;
            foreach (var transaction in depositTransactions)
            {
                var splitTransaction = transaction.Split(transactionSpliter, StringSplitOptions.RemoveEmptyEntries);
                entryCounter++;
                statementEntries.Add(new StatementEntry
                {
                    Amount = new AmountDetail
                    {
                        Amount = Convert.ToDecimal(splitTransaction[2]),
                        Currency = CurrencyCode.USD
                    },
                    Category = CategoryType.Income,
                    EntryBaseType = TransactionType.Credit,
                    Description = splitTransaction[1],
                    Date = parseDate(splitTransaction[0]),
                    CheckNumber = string.Empty,
                    EntryNumber = entryCounter.ToString(),
                    EndingDailyBalance = 0,
                    IsServiceFee = false,
                    MerchantName = string.Empty
                });
            }

            var atmAndDebitCardTransactions = ruleResults[BankRuleName.AtmAndDebitCardTransactions];
            entryCounter = 0;
            foreach (var transaction in atmAndDebitCardTransactions)
            {
                var splitTransaction = transaction.Split(transactionSpliter, StringSplitOptions.RemoveEmptyEntries);
                entryCounter++;
                statementEntries.Add(new StatementEntry
                {
                    Amount = new AmountDetail
                    {
                        Amount = Convert.ToDecimal(splitTransaction[2]),
                        Currency = CurrencyCode.USD
                    },
                    Category = CategoryType.Expense,
                    EntryBaseType = TransactionType.Debit,
                    Description = splitTransaction[1],
                    Date = parseDate(splitTransaction[0]),
                    CheckNumber = string.Empty,
                    EntryNumber = entryCounter.ToString(),
                    EndingDailyBalance = 0,
                    IsServiceFee = false,
                    MerchantName = string.Empty
                });
            }

            var otherTransactions = ruleResults[BankRuleName.AtmAndDebitCardTransactions];
            entryCounter = 0;
            foreach (var transaction in otherTransactions)
            {
                var splitTransaction = transaction.Split(transactionSpliter, StringSplitOptions.RemoveEmptyEntries);
                entryCounter++;
                statementEntries.Add(new StatementEntry
                {
                    Amount = new AmountDetail
                    {
                        Amount = Convert.ToDecimal(splitTransaction[2]),
                        Currency = CurrencyCode.USD
                    },
                    Category = CategoryType.Expense,
                    EntryBaseType = TransactionType.Debit,
                    Description = splitTransaction[1],
                    Date = parseDate(splitTransaction[0]),
                    CheckNumber = string.Empty,
                    EntryNumber = entryCounter.ToString(),
                    EndingDailyBalance = 0,
                    IsServiceFee = false,
                    MerchantName = string.Empty
                });
            }


            var serviceFeeTransactions = ruleResults[BankRuleName.AtmAndDebitCardTransactions];
            entryCounter = 0;
            foreach (var transaction in serviceFeeTransactions)
            {
                var splitTransaction = transaction.Split(transactionSpliter, StringSplitOptions.RemoveEmptyEntries);
                entryCounter++;
                statementEntries.Add(new StatementEntry
                {
                    Amount = new AmountDetail
                    {
                        Amount = Convert.ToDecimal(splitTransaction[2]),
                        Currency = CurrencyCode.USD
                    },
                    Category = CategoryType.Expense,
                    EntryBaseType = TransactionType.Debit,
                    Description = splitTransaction[1],
                    Date = parseDate(splitTransaction[0]),
                    CheckNumber = string.Empty,
                    EntryNumber = entryCounter.ToString(),
                    EndingDailyBalance = 0,
                    IsServiceFee = true,
                    MerchantName = string.Empty
                });
            }
            return statementEntries;
        }

        private Customer TransformToCustomer(IDictionary<string, IEnumerable<string>> ruleResults)
        {
            var customerName = TransformToCustomerName(ruleResults[BankRuleName.CustomerName]);
            var splitCustomerName = customerName.Split(' ');

            return new Customer
            {
                CustomerNumber = string.Empty,
                Address = TransformToAddress(ruleResults[BankRuleName.CustomerAddress]),
                CustomerName = new List<CustomerNameDetail>
                {
                    new CustomerNameDetail
                    {
                        NameOnTheAccount = customerName,
                        Position = AccountHolderPosition.Primary,
                        FirstName = splitCustomerName.Length > 1 ? splitCustomerName[0] : customerName,
                        LastName = splitCustomerName.Length > 2 ? splitCustomerName[2] : splitCustomerName.Length > 1 ? splitCustomerName[1] : string.Empty,
                        MI = splitCustomerName.Length > 2 ? splitCustomerName[1] : string.Empty,
                        Generation = "Senior"
                    }
                }
            };
        }

        private string TransformToCustomerName(IEnumerable<string> ruleResults)
        {
            return ruleResults.First();
        }

        private BankStatement TransformToBankStatement(IDictionary<string, IEnumerable<string>> ruleResults)
        {
            // Extract month and year
            var matches = Regex.Matches(ruleResults[BankRuleName.StatementMonth].First(), MonthYearPattern);



            var bankStatement = new BankStatement
            {
                Bank = new Bank
                {
                    Name = BankName.Get(BankName.BankOfAmerica),
                    Address = TransformToAddress(ruleResults[BankRuleName.BankAddress]),
                    RoutingNumber = string.Empty
                },
                CustomerReferenceNumber = Guid.NewGuid().ToString(),
                FromMonth =
                    (CashflowMonth) Enum.Parse(typeof (CashflowMonth), matches[0].Groups["month"].Value.Substring(0, 3)),
                ToMonth =
                    (CashflowMonth) Enum.Parse(typeof (CashflowMonth), matches[1].Groups["month"].Value.Substring(0, 3)),
                FromYear = int.Parse(matches[0].Groups["year"].Value),
                ToYear = int.Parse(matches[1].Groups["year"].Value)
            };

            return bankStatement;
        }

        private BankAccount TranformToBankAccount(IDictionary<string, IEnumerable<string>> ruleResults)
        {
            return new BankAccount
            {
                AccountNumber = TransformToAccountNumber(ruleResults[BankRuleName.AccountNumber]),
                AccountType = TransformToAccountType(ruleResults),
                BeginningBalance = TransformToBalance(ruleResults[BankRuleName.BeginningBalance]),
                EndingBalance = TransformToBalance(ruleResults[BankRuleName.EndingBalance]),
                StatementEntries = TransformToStatementEntries(ruleResults),
                IsActive = true,
                IsPrimary = true
            };
        }

        private decimal TransformToBalance(IEnumerable<string> ruleResults)
        {
            return Convert.ToDecimal(Regex.Match(ruleResults.First(), @"([+\-]*\d+[.,]+\d+[.,]*\d+)").Value);
        }

        private bool IsTrueFor(string ruleName, IDictionary<string, IEnumerable<string>> ruleResults)
        {
            IEnumerable<string> values;
            if (ruleResults.TryGetValue(ruleName, out values))
            {
                return Convert.ToBoolean(values.First());
            }

            return false;
        }

        private BankAccountType TransformToAccountType(IDictionary<string, IEnumerable<string>> ruleResults)
        {
            if (IsTrueFor(BankRuleName.IsSavingAccount, ruleResults))
            {
                return BankAccountType.Saving;
            }

            if (IsTrueFor(BankRuleName.IsCheckingAccount, ruleResults))
            {
                return BankAccountType.Checking;
            }

            return BankAccountType.Others;
        }

        private string TransformToAccountNumber(IEnumerable<string> ruleResults)
        {
            return ruleResults.First().Replace(" ", string.Empty);
        }

        private Address TransformToAddress(IEnumerable<string> ruleResults)
        {
            return new Address
            {
                Address1 = ruleResults.First(),
                Address2 = ruleResults.Last(),
                Country = "USA",
                City = "NA",
                State = "NA",
                ZipCode = "00000"
            };
        }
    }
}