﻿using System.Threading.Tasks;
using LendFoundry.Ocr.Engine.Cashflow;

namespace LendFoundry.Ocr.Engine.TemplateProcessors
{
    public interface IBankEngine
    {
        Task<BankStatement> HandleStatement(string statementPath);
    }
}