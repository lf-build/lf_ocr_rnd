﻿using System.Threading.Tasks;
using LendFoundry.Ocr.Engine.Cashflow;
using LendFoundry.Ocr.Engine.Formatters.OcrXml;

namespace LendFoundry.Ocr.Engine.TemplateProcessors
{
    public class EmptyBankProcessor : IBankProcessor
    {
        public Task<BankStatement> ProcessStatement(Container statement)
        {
            return Task.FromResult(new BankStatement());
        }
    }
}