﻿namespace LendFoundry.Ocr.Engine.Cashflow
{
    public class CustomerNameDetail
    {
        public string NameOnTheAccount { get; set; } //Required field
        public string FirstName { get; set; }
        public string MI { get; set; }
        public string LastName { get; set; }
        public string Generation { get; set; } /*Sr, Jr*/
        public AccountHolderPosition Position { get; set; } //Required field
    }
}