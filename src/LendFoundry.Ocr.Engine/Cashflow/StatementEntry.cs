using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace LendFoundry.Ocr.Engine.Cashflow
{
    public class StatementEntry
    {
        public string CheckNumber { get; set; }
        public AmountDetail Amount { get; set; } //Required field
        public decimal? EndingDailyBalance { get; set; }
        public DateTime Date { get; set; } //Required field
        public string MerchantName { get; set; }
        public string EntryNumber { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public CategoryType Category { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public TransactionType EntryBaseType { get; set; } //Required field
        public bool IsServiceFee { get; set; }
        public string Description { get; set; } //Required field
    }
}