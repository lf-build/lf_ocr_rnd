﻿using System.Collections.Generic;

namespace LendFoundry.Ocr.Engine.Cashflow
{
    public class Customer
    {
        public string CustomerNumber { get; set; }
        public List<CustomerNameDetail> CustomerName { get; set; } = new List<CustomerNameDetail>();
        public Address Address { get; set; }
    }
}