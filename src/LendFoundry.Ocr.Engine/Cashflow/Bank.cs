﻿namespace LendFoundry.Ocr.Engine.Cashflow
{
    public class Bank
    {
        public string Name { get; set; } //Required field
        public string RoutingNumber { get; set; }
        public Address Address { get; set; }
    }
}