namespace LendFoundry.Ocr.Engine.Cashflow
{
    public class Address
    {
        public string Address1 { get; set; } //Required field
        public string Address2 { get; set; }
        public string City { get; set; } //Required field
        public string State { get; set; } //Required field
        public string ZipCode { get; set; } //Required field
        public string Country { get; set; } //Required field
    }
}