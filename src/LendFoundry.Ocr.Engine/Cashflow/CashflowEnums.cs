﻿namespace LendFoundry.Ocr.Engine.Cashflow
{
    public enum AccountHolderPosition
    {
        Primary = 1,
        Secondary = 2,
        Third = 3
    }

    public enum BankAccountType
    {
        Checking = 1,
        Saving = 2,
        CertificateOfDeposit = 3,
        CreditCard = 4,
        Others = 99
    }

    public enum CashflowMonth
    {
        Jan = 1,
        Feb = 2,
        Mar = 3,
        Apr = 4,
        May = 5,
        Jun = 6,
        Jul = 7,
        Aug = 8,
        Sep = 9,
        Oct = 10,
        Nov = 11,
        Dec = 12
    }

    public enum CategoryType
    {
        Uncategorize = 1,
        Expense = 2,
        Transfer = 3,
        Income = 4,
        DeferredCompensation = 5,
        Others = 999
    }

    public enum CurrencyCode
    {
        USD = 1,
        INR = 91,
        Others = 999
    }

    public enum TransactionType
    {
        Credit = 1,
        Debit = 2,
        Others = 99
    }
}