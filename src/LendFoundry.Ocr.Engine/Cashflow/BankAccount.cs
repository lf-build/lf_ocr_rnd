﻿using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace LendFoundry.Ocr.Engine.Cashflow
{
    public class BankAccount
    {
        public string AccountNumber { get; set; } //Required field
        [JsonConverter(typeof(StringEnumConverter))]
        public BankAccountType AccountType { get; set; } //Required field
        public decimal BeginningBalance { get; set; } //Required field
        public decimal EndingBalance { get; set; } //Required field
        public bool IsActive { get; set; }
        public bool IsPrimary { get; set; }
        public Customer Customer { get; set; }
        public List<StatementEntry> StatementEntries { get; set; } = new List<StatementEntry>();
    }
}