﻿namespace LendFoundry.Ocr.Engine.Cashflow
{
    public class AmountDetail
    {
        public decimal Amount { get; set; } //Required field
        public CurrencyCode Currency { get; set; } //Required field
    }
}