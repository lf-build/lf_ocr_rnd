﻿using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace LendFoundry.Ocr.Engine.Cashflow
{
    public class BankStatement
    {
        public Bank Bank { get; set; }
        [JsonProperty("Accounts")]
        public List<BankAccount> BankAccounts { get; set; } = new List<BankAccount>();
        public decimal TotalBalance { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public CashflowMonth FromMonth { get; set; }
        public int FromYear { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public CashflowMonth ToMonth { get; set; }
        public int ToYear { get; set; }
        public string CustomerReferenceNumber { get; set; } //Required field
    }
}